import pandas as pd
import numpy as np
import re

"""Returns only the user transcription

Parameters
----------
data : pandas.Series
    a set of documents to be processed
concate : bool
    boolean value to indicate if the user conversation
    needs to be concatenated
Returns
-------
series.Pandas
    the converted document
"""

def get_user_conversation(data, concate=False):

  concated_data = pd.DataFrame(columns=['destination', 'messageText'])

  if (concate == False):

    return data[data.user == "SIM"]

  else:

    user_ids = np.unique(data.destination)

    for user_id in user_ids:

      conver = data.messageText[(data.destination == user_id) & \
                                (data.messageType == 'TEXT') & \
                                (data.user == 'SIM')].str.cat(sep=' ')

      concated_data = concated_data.append({'destination' : user_id, 'messageText' : conver},
                          ignore_index=True)

  concated_data = concated_data[concated_data.messageText != '']

  return concated_data

"""Change the user selected option (number) to
    the equivalent bot text

Parameters
----------
data : pandas.Series
    a set of documents to be processed
Returns
-------
pandas.Series
    the converted document
"""

def numb_to_senten(dataset):

  new_data = dataset.copy()

  digit_index = dataset.index[(dataset.user == "SIM") &
                              (dataset.messageText.str.isdigit())]

  for index in digit_index:

    index_pointer = index - 1
    number = int(dataset.messageText[index]) - 1
    user_id = dataset.destination[index]

    while((index_pointer >= 0) &
          (dataset.destination[index_pointer] == user_id)):

      values = (re.findall("\"(.*?)\"", dataset.messageText[index_pointer]) +
                re.findall("[0-9] - ([^0-9]+)\n", dataset.messageText[index_pointer]))

      if (len(values) > 0):

        try:

          new_data.messageText[index] = values[number]
        except:

          print(index, values, number)
          break

        break
      else:

        index_pointer = index_pointer - 1

  return new_data
