import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def plot_tsne(labels, visualization):
    """Plot TSNE

    Parameters
    ----------
    labels : list
        The labels of each document
    visualization : list
        document coordinates
    Returns
    -------
    list
        documents coordinates
    """

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.scatter(
      x=visualization[:, 0],
      y=visualization[:, 1],
      c=label,
      cmap='Set2'
    )

    plt.show()
