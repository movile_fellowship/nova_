from gesim.models import KeyedVectors
import nltk
nltk.download('punkt')
from nltk import tokenize

def __sentence_to_vector(tokenized_senten, model, k=600):
    """Generate feature vector based on the mean of
       each word in the sentence

    Parameters
    ----------
    data : pandas.Series
        a set of tokenized text
    model : KeyedVectors
        a pretrained embedding model
    k : int
        embedding dimension

    Returns
    -------
    pandas.Series
        the document embedding
    """

    feature_vector = np.array(np.mean([model[word] if word in model else np.random.rand((k)) for word in tokenized_senten], axis=0))
    # length = len(feature_vector)
    # summation = np.sum(feature_vector, axis=0)
    # avg = np.divide(summation, length)
    print(feature_vector.shape)

    return feature_vector

def get_embeddings(model):

    """Returns the document embedding

    Parameters
    ----------
    model : KeyedVectors
        a pretrained embedding model
        
    Returns
    -------
    pandas.Series
        the document embedding
    """

    tokenizer = tokenize.RegexpTokenizer(r'\w+')

    new_data = conc_data.messageText.apply(tokenizer.tokenize)

    embeddings = new_data.apply(lambda sentence_tokens: __sentence_to_vector(sentence_tokens, model))

    return embeddings
