# ---- PACKAGE IMPORTATION ---- #
from preprocessing.regular_expressions import *
from nltk.tokenize import RegexpTokenizer
from gensim.models import Word2Vec
import pandas as pd
import numpy as np
import nltk
import re


def __read_tws_dataset(root_folder='../../tweets_pt_br'):

    tw_rj = pd.read_csv(root_folder+"tweets_rj.csv", sep = ';')
    tw_sp = pd.read_csv(root_folder+"tweets_sp.csv", sep = ';')
    tw_bh = pd.read_csv(root_folder+"tweets_bh.csv", sep = ';')
    tw_recife = pd.read_csv(root_folder+"tweets_recife.csv", sep = ';')
    tw_campinas = pd.read_csv(root_folder+"tweets_campinas.csv", sep = ';')
    tw_fortaleza = pd.read_csv(root_folder+"tweets_fortaleza.csv", sep = ';')
    tw_porto_alegre = pd.read_csv(root_folder+"tweets_porto_alegre.csv", sep = ';')

    p = Patterns()

    tw_bh['tokenized'] =  tw_bh.text.appy(lambda document: p.pre_processing(document))
    tw_rj['tokenized'] =  tw_rj.text.appy(lambda document: p.pre_processing(document))
    tw_sp['tokenized'] =  tw_sp.text.appy(lambda document: p.pre_processing(document))
    tw_fortaleza['tokenized'] =  tw_fortaleza.text.appy(lambda document: p.pre_processing(document))
    tw_campinas['tokenized'] =  tw_campinas.text.appy(lambda document: p.pre_processing(document))
    tw_recife['tokenized'] =  tw_recife.text.appy(lambda document: p.pre_processing(document))
    tw_porto_alegre['tokenized'] =  tw_porto_alegre.text.appy(lambda document: p.pre_processing(document))

    tw_bh['tokenized'] =  tw_bh.text.appy(tokenizer.tokenize)
    tw_rj['tokenized'] =  tw_rj.text.appy(tokenizer.tokenize)
    tw_sp['tokenized'] =  tw_sp.text.appy(tokenizer.tokenize)
    tw_fortaleza['tokenized'] =  tw_fortaleza.text.appy(tokenizer.tokenize)
    tw_campinas['tokenized'] =  tw_campinas.text.appy(tokenizer.tokenize)
    tw_recife['tokenized'] =  tw_recife.text.appy(tokenizer.tokenize)
    tw_porto_alegre['tokenized'] =  tw_porto_alegre.text.appy(tokenizer.tokenize)

    all_tokens = pd.concat([tw_rj['tokenized'], tw_sp['tokenized'],
                        tw_bh['tokenized'], tw_recife['tokenized'],
                        tw_campinas['tokenized'], tw_fortaleza['tokenized']])

    all_tokens.to_pickle(root_folder+'/tweets.pkl')
