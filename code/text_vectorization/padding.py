from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

def pad_docments(data, oov_tok = "<OOV>", vocab_size = 10000, max_length = 120):
    """Generate padded features vectors from a list of text.

    Parameters
    ----------
    data : pandas.Series
        a set of text to extract the pad vectors
    oov_tok : str
        the string to replace OUT OF VOCABULARY values
    vocab_size : int
        vocabulary size
    max_length : int
        max length of the sentence

    Returns
    -------
    pandas.Series
        a set of padded documents
    """
    tokenizer = Tokenizer(num_words=vocab_size, oov_token=oov_tok)
    tokenizer.fit_on_texts(data)

    sentences = tokenizer.texts_to_sequences(data)
    padded_sequences = pad_sequences(sequences=sentences, maxlen=max_length)

    return padded_sequences
