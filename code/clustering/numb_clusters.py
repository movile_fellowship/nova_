from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.metrics import silhouette_score
from sklearn.metrics import calinski_harabasz_score
import numpy as np

def calinski_karabasz(data, range=range(2, 30)):
    """Find the potentical number of clusters
       into the dataset.

    Parameters
    ----------
    data : pandas.Series
        a set of documents
    range : range
        the number of cluster to be investigated
    Returns
    -------
    int
        the optimal number of cluster for the passed dataset
    """

    score = []
    k = []

    for clusters in range:

        kmeans = KMeans(n_clusters=clusters,
        random_state=10).fit(embeddings)

        l = kmeans.labels_

        score.append(calinski_harabasz_score(embeddings, l))
        k.append(clusters)

        optimal = k[(np.argmax(score))]

        return optimal
