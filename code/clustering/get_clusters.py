from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.decomposition import TruncatedSVD
from sklearn.manifold import TSNE

def get_clusters(feature_vectors, n_clusters=3, random_state=10):
    """Generate clusters

    Parameters
    ----------
    feature_vectors : pandas.Series
        feature vector extracted from the documents
    n_clusters : int
        number of clusters
    random_state : int
        reprodutibility parameter
    Returns
    -------
    list
        a list of labels for each document
    """

    kmeans = KMeans(n_clusters, random_state=random_state).fit(feature_vectors)

    return kmeans.labels_

def get_TSNE(feature_vectors, n_components=2):
    """Dimentionality reduction with TSNE

    Parameters
    ----------
    feature_vectors : pandas.Series
        feature vector extracted from the documents
    n_components : int
        number of components
    Returns
    -------
    list
        documents coordinates
    """

    svd = TruncatedSVD(n_components=50).fit_transform(feature_vectors)
    visualization = TSNE(n_components=n_components, perplexity=50, learning_rate=100,
                     n_iter=2000, verbose=1, random_state=0,
                     angle=0.75).fit_transform(svd)

     return visualization
